local TMW                                   = TMW
local _G, type, error, time     			= _G, type, error, time
local A                         			= _G.Action
local TeamCache								= A.TeamCache
local EnemyTeam								= A.EnemyTeam
local FriendlyTeam							= A.FriendlyTeam
local LoC									= A.LossOfControl
local Player								= A.Player 
local MultiUnits							= A.MultiUnits
local UnitCooldown							= A.UnitCooldown
local ActiveUnitPlates						= MultiUnits:GetActiveUnitPlates()
local toStr                     			= A.toStr
local toNum                     			= A.toNum
local LibToast                              = LibStub("LibToast-1.0")
local next, pairs, type, print              = next, pairs, type, print
local IsActionInRange, GetActionInfo, PetHasActionBar, GetPetActionsUsable, GetSpellInfo = IsActionInRange, GetActionInfo, PetHasActionBar, GetPetActionsUsable, GetSpellInfo
local UnitLevel, UnitPower, UnitPowerMax, UnitStagger, UnitAttackSpeed, UnitRangedDamage, UnitDamage, UnitAura = UnitLevel, UnitPower, UnitPowerMax, UnitStagger, UnitAttackSpeed, UnitRangedDamage, UnitDamage, UnitAura
local UnitIsPlayer, UnitExists, UnitGUID    = UnitIsPlayer, UnitExists, UnitGUID
--local Pet                                 = LibStub("PetLibrary") Don't work. Too fast loading snippets ?
local Unit                                  = A.Unit 
local huge                                  = math.huge
local UnitBuff                              = _G.UnitBuff
local EventFrame                            = CreateFrame("Frame", "Taste_EventFrame", UIParent)
local UnitIsUnit                            = UnitIsUnit
local StdUi                                 = A.StdUi -- Custom StdUI with Action shared settings
-- Lua methods
local error                                 = error
local setmetatable 						    = setmetatable
local stringformat 						    = string.format
local stringfind                            = string.find
local stringsub                             = string.sub
local tableinsert 						    = table.insert
local tableremove							= table.remove 
local TR                                    = Action.TasteRotation



------------------------------------------
-- Castle Nathria DeadlyBossMods timers --
------------------------------------------
-- global vars
TR.BossModsDontUseImportants = false

-- core
function TR.CastleNathria()
    
    -- Shriekwing
	local WarnBloodgorge = A.BossMods:GetTimer(328921)
    if WarnBloodgorge > 0 and WarnBloodgorge <= 10 then -- Stage 2 detected in 10sec
        TR.BossModsDontUseImportants = true -- global var to tell the rotation we are gonna change phase soon so don't waste pot or cooldowns
	end
	
    -- Hungering Destroyer

    -- Kael'thas Sunstrider

    -- Artificer Xy'Mox

    -- Lady Inverva Darkvein

    -- The Council of Blood

    -- Sludgefist

    -- Stoneborne Generals

    -- Sire Denathrius



------------------------------------------------
-- ShadowLands Dungeons DeadlyBossMods timers --
------------------------------------------------

-- De Other Side

-- Halls of Atonement

-- Mists of Tirna Scithe

-- Necrotic Wake

-- Plaguefall

-- Sanguine Depths

-- Spires of Ascension

-- Theater of Pain

